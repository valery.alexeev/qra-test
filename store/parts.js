export const state = () => ({
	active: null
})

export const mutations = {
	set(state, id) {
		state.active = id;
	},
	unset(state) {
		state.active = null;
	}
}