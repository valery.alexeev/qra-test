export const state = () => ({
	is_shown: false
})

export const mutations = {
	show(state) {
		state.is_shown = true;
	},
	hide(state) {
		state.is_shown = false;
	}
}